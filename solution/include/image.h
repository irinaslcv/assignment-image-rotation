#ifndef IMAGE
#define IMAGE

#include <stdint.h>


struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void init_image(uint64_t height, uint64_t width, struct image* img);



#endif
